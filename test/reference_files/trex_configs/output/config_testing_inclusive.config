% ------- %
% - JOB - %
% ------- %

Job: "my_fit"
	HistoPath: /builds/atlas-amglab/fastframes
	Lumi: 1
	ImageFormat: pdf
	ReadFrom: HIST
	POI: mu_signal
	HistoChecks: NOCRASH

% ------- %
% - FIT - %
% ------- %

Fit: "fit"
	FitType: SPLUSB
	POIAsimov: 1
	FitRegion: CRSR
	FitBlind: True

% ----------- %
% - REGIONS - %
% ----------- %

Region: "Electron_jet_pt"
	Type: SIGNAL
	VariableTitle: jet_pt
	HistoName: NOSYS/jet_pt_Electron
	Label: Electron_jet_pt
	ShortLabel: Electron_jet_pt

Region: "Electron_met_met"
	Type: SIGNAL
	VariableTitle: met_met
	HistoName: NOSYS/met_met_Electron
	Label: Electron_met_met
	ShortLabel: Electron_met_met

Region: "Electron_met_phi"
	Type: SIGNAL
	VariableTitle: met_phi
	HistoName: NOSYS/met_phi_Electron
	Label: Electron_met_phi
	ShortLabel: Electron_met_phi

Region: "Muon_jet_pt"
	Type: SIGNAL
	VariableTitle: jet_pt
	HistoName: NOSYS/jet_pt_Muon
	Label: Muon_jet_pt
	ShortLabel: Muon_jet_pt

Region: "Muon_met_met"
	Type: SIGNAL
	VariableTitle: met_met
	HistoName: NOSYS/met_met_Muon
	Label: Muon_met_met
	ShortLabel: Muon_met_met

Region: "Muon_met_phi"
	Type: SIGNAL
	VariableTitle: met_phi
	HistoName: NOSYS/met_phi_Muon
	Label: Muon_met_phi
	ShortLabel: Muon_met_phi

% ----------- %
% - SAMPLES - %
% ----------- %

Sample: "ttbar_FS"
	Type: BACKGROUND
	Title: ttbar_FS
	HistoFile: ttbar_FS
	FillColor: 3
	LineColor: 3

Sample: "Zjets_light"
	Type: BACKGROUND
	Title: Zjets_light
	HistoFile: Zjets_light
	FillColor: 4
	LineColor: 4
	Exclude: Electron_jet_pt,Muon_jet_pt

Sample: "Zjets_b"
	Type: BACKGROUND
	Title: Zjets_b
	HistoFile: Zjets_b
	FillColor: 5
	LineColor: 5

Sample: "Wjets_FS"
	Type: BACKGROUND
	Title: Wjets_FS
	HistoFile: Wjets_FS
	FillColor: 6
	LineColor: 6
	Exclude: Electron_jet_pt

Sample: "Data"
	Type: DATA
	Title: Data
	HistoFile: Data
	FillColor: 7
	LineColor: 7
	Regions: Electron_met_met,Muon_met_met

% ----------------- %
% - NORM. FACTORS - %
% ----------------- %

NormFactor: "mu_signal"
	Title: ""#mu(signal)""
	Nominal: 1
	Min: -100
	Max: 100
	Samples: ttbar_FS

% --------------- %
% - SYSTEMATICS - %
% --------------- %

Systematic: "btag_B_1"
	HistoFolderNameUp: btag_B_1__up
	HistoFolderNameDown: btag_B_1__down
	Title: "btag B 1"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40
	Exclude: ttbar_FS

Systematic: "btag_C_1"
	HistoFolderNameUp: btag_C_1__up
	HistoFolderNameDown: btag_C_1__down
	Title: "btag C 1"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40
	Regions: Electron_jet_pt,Electron_met_met,Electron_met_phi
	Samples: Zjets_b,Wjets_FS

Systematic: "btag_light_1"
	HistoFolderNameUp: btag_light_1__up
	HistoFolderNameDown: btag_light_1__down
	Title: "btag light 1"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40
	Regions: Electron_jet_pt,Electron_met_met,Electron_met_phi
	Samples: Zjets_b,Wjets_FS

Systematic: "btag_light_2"
	HistoFolderNameUp: btag_light_2__up
	HistoFolderNameDown: btag_light_2__down
	Title: "btag light 2"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40
	Regions: Electron_jet_pt,Electron_met_met,Electron_met_phi
	Samples: Zjets_b,Wjets_FS

Systematic: "btag_light_3"
	HistoFolderNameUp: btag_light_3__up
	HistoFolderNameDown: btag_light_3__down
	Title: "btag light 3"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40
	Regions: Electron_jet_pt,Electron_met_met,Electron_met_phi
	Samples: Zjets_b,Wjets_FS

Systematic: "btag_light_4"
	HistoFolderNameUp: btag_light_4__up
	HistoFolderNameDown: btag_light_4__down
	Title: "btag light 4"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40
	Regions: Electron_jet_pt,Electron_met_met,Electron_met_phi
	Samples: Zjets_b,Wjets_FS

Systematic: "bootstraps1"
	HistoFolderNameUp: bootstraps1
	Title: bootstraps1
	Type: HISTO
	Symmetrisation: ONESIDED
	Smoothing: 40
	Samples: Data

Systematic: "EG_RESOLUTION_ALL"
	HistoFolderNameDown: EG_RESOLUTION_ALL__1down
	Samples: ttbar_FS
	HistoFolderNameUp: EG_RESOLUTION_ALL__1up
	Title: "EG RESOLUTION ALL"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "EG_SCALE_AF2"
	HistoFolderNameDown: EG_SCALE_AF2__1down
	Samples: ttbar_FS
	HistoFolderNameUp: EG_SCALE_AF2__1up
	Title: "EG SCALE AF2"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "EG_SCALE_ALL"
	HistoFolderNameDown: EG_SCALE_ALL__1down
	Samples: ttbar_FS
	HistoFolderNameUp: EG_SCALE_ALL__1up
	Title: "EG SCALE ALL"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR"
	HistoFolderNameDown: EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down
	Samples: ttbar_FS
	HistoFolderNameUp: EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up
	Title: "EL EFF ID TOTAL 1NPCOR PLUS UNCOR"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR"
	HistoFolderNameDown: EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down
	Samples: ttbar_FS
	HistoFolderNameUp: EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up
	Title: "EL EFF Iso TOTAL 1NPCOR PLUS UNCOR"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR"
	HistoFolderNameDown: EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down
	Samples: ttbar_FS
	HistoFolderNameUp: EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up
	Title: "EL EFF Reco TOTAL 1NPCOR PLUS UNCOR"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR"
	HistoFolderNameDown: EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down
	Samples: ttbar_FS
	HistoFolderNameUp: EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up
	Title: "EL EFF TriggerEff TOTAL 1NPCOR PLUS UNCOR"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "FT_EFF_B_systematics"
	HistoFolderNameDown: FT_EFF_B_systematics__1down
	Samples: ttbar_FS
	HistoFolderNameUp: FT_EFF_B_systematics__1up
	Title: "FT EFF B systematics"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "FT_EFF_C_systematics"
	HistoFolderNameDown: FT_EFF_C_systematics__1down
	Samples: ttbar_FS
	HistoFolderNameUp: FT_EFF_C_systematics__1up
	Title: "FT EFF C systematics"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "FT_EFF_Light_systematics"
	HistoFolderNameDown: FT_EFF_Light_systematics__1down
	Samples: ttbar_FS
	HistoFolderNameUp: FT_EFF_Light_systematics__1up
	Title: "FT EFF Light systematics"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "FT_EFF_extrapolation"
	HistoFolderNameDown: FT_EFF_extrapolation__1down
	Samples: ttbar_FS
	HistoFolderNameUp: FT_EFF_extrapolation__1up
	Title: "FT EFF extrapolation"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "FT_EFF_extrapolation_from_charm"
	HistoFolderNameDown: FT_EFF_extrapolation_from_charm__1down
	Samples: ttbar_FS
	HistoFolderNameUp: FT_EFF_extrapolation_from_charm__1up
	Title: "FT EFF extrapolation from charm"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_BJES_Response"
	HistoFolderNameUp: JET_BJES_Response__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_BJES_Response__1down
	Title: "JET BJES Response"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_EffectiveNP_Detector1"
	HistoFolderNameUp: JET_EffectiveNP_Detector1__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_EffectiveNP_Detector1__1down
	Title: "JET EffectiveNP Detector1"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_EffectiveNP_Detector2"
	HistoFolderNameUp: JET_EffectiveNP_Detector2__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_EffectiveNP_Detector2__1down
	Title: "JET EffectiveNP Detector2"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_EffectiveNP_Mixed1"
	HistoFolderNameUp: JET_EffectiveNP_Mixed1__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_EffectiveNP_Mixed1__1down
	Title: "JET EffectiveNP Mixed1"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_EffectiveNP_Mixed2"
	HistoFolderNameUp: JET_EffectiveNP_Mixed2__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_EffectiveNP_Mixed2__1down
	Title: "JET EffectiveNP Mixed2"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_EffectiveNP_Mixed3"
	HistoFolderNameUp: JET_EffectiveNP_Mixed3__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_EffectiveNP_Mixed3__1down
	Title: "JET EffectiveNP Mixed3"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_EffectiveNP_Modelling1"
	HistoFolderNameUp: JET_EffectiveNP_Modelling1__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_EffectiveNP_Modelling1__1down
	Title: "JET EffectiveNP Modelling1"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_EffectiveNP_Modelling2"
	HistoFolderNameUp: JET_EffectiveNP_Modelling2__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_EffectiveNP_Modelling2__1down
	Title: "JET EffectiveNP Modelling2"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_EffectiveNP_Modelling3"
	HistoFolderNameUp: JET_EffectiveNP_Modelling3__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_EffectiveNP_Modelling3__1down
	Title: "JET EffectiveNP Modelling3"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_EffectiveNP_Modelling4"
	HistoFolderNameUp: JET_EffectiveNP_Modelling4__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_EffectiveNP_Modelling4__1down
	Title: "JET EffectiveNP Modelling4"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_EffectiveNP_Statistical1"
	HistoFolderNameUp: JET_EffectiveNP_Statistical1__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_EffectiveNP_Statistical1__1down
	Title: "JET EffectiveNP Statistical1"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_EffectiveNP_Statistical2"
	HistoFolderNameUp: JET_EffectiveNP_Statistical2__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_EffectiveNP_Statistical2__1down
	Title: "JET EffectiveNP Statistical2"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_EffectiveNP_Statistical3"
	HistoFolderNameUp: JET_EffectiveNP_Statistical3__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_EffectiveNP_Statistical3__1down
	Title: "JET EffectiveNP Statistical3"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_EffectiveNP_Statistical4"
	HistoFolderNameUp: JET_EffectiveNP_Statistical4__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_EffectiveNP_Statistical4__1down
	Title: "JET EffectiveNP Statistical4"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_EffectiveNP_Statistical5"
	HistoFolderNameUp: JET_EffectiveNP_Statistical5__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_EffectiveNP_Statistical5__1down
	Title: "JET EffectiveNP Statistical5"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_EffectiveNP_Statistical6"
	HistoFolderNameUp: JET_EffectiveNP_Statistical6__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_EffectiveNP_Statistical6__1down
	Title: "JET EffectiveNP Statistical6"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_EtaIntercalibration_Modelling"
	HistoFolderNameUp: JET_EtaIntercalibration_Modelling__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_EtaIntercalibration_Modelling__1down
	Title: "JET EtaIntercalibration Modelling"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_EtaIntercalibration_NonClosure_highE"
	HistoFolderNameUp: JET_EtaIntercalibration_NonClosure_highE__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_EtaIntercalibration_NonClosure_highE__1down
	Title: "JET EtaIntercalibration NonClosure highE"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_EtaIntercalibration_NonClosure_negEta"
	HistoFolderNameUp: JET_EtaIntercalibration_NonClosure_negEta__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_EtaIntercalibration_NonClosure_negEta__1down
	Title: "JET EtaIntercalibration NonClosure negEta"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_EtaIntercalibration_NonClosure_posEta"
	HistoFolderNameUp: JET_EtaIntercalibration_NonClosure_posEta__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_EtaIntercalibration_NonClosure_posEta__1down
	Title: "JET EtaIntercalibration NonClosure posEta"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_EtaIntercalibration_TotalStat"
	HistoFolderNameUp: JET_EtaIntercalibration_TotalStat__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_EtaIntercalibration_TotalStat__1down
	Title: "JET EtaIntercalibration TotalStat"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_Flavor_Composition"
	HistoFolderNameUp: JET_Flavor_Composition__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_Flavor_Composition__1down
	Title: "JET Flavor Composition"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_Flavor_Response"
	HistoFolderNameUp: JET_Flavor_Response__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_Flavor_Response__1down
	Title: "JET Flavor Response"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_JER_DataVsMC_MC16"
	HistoFolderNameUp: JET_JER_DataVsMC_MC16__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_JER_DataVsMC_MC16__1down
	Title: "JET JER DataVsMC MC16"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_JER_EffectiveNP_1"
	HistoFolderNameUp: JET_JER_EffectiveNP_1__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_JER_EffectiveNP_1__1down
	Title: "JET JER EffectiveNP 1"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_JER_EffectiveNP_10"
	HistoFolderNameUp: JET_JER_EffectiveNP_10__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_JER_EffectiveNP_10__1down
	Title: "JET JER EffectiveNP 10"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_JER_EffectiveNP_11"
	HistoFolderNameUp: JET_JER_EffectiveNP_11__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_JER_EffectiveNP_11__1down
	Title: "JET JER EffectiveNP 11"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_JER_EffectiveNP_12restTerm"
	HistoFolderNameUp: JET_JER_EffectiveNP_12restTerm__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_JER_EffectiveNP_12restTerm__1down
	Title: "JET JER EffectiveNP 12restTerm"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_JER_EffectiveNP_2"
	HistoFolderNameUp: JET_JER_EffectiveNP_2__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_JER_EffectiveNP_2__1down
	Title: "JET JER EffectiveNP 2"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_JER_EffectiveNP_3"
	HistoFolderNameUp: JET_JER_EffectiveNP_3__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_JER_EffectiveNP_3__1down
	Title: "JET JER EffectiveNP 3"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_JER_EffectiveNP_4"
	HistoFolderNameUp: JET_JER_EffectiveNP_4__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_JER_EffectiveNP_4__1down
	Title: "JET JER EffectiveNP 4"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_JER_EffectiveNP_5"
	HistoFolderNameUp: JET_JER_EffectiveNP_5__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_JER_EffectiveNP_5__1down
	Title: "JET JER EffectiveNP 5"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_JER_EffectiveNP_6"
	HistoFolderNameUp: JET_JER_EffectiveNP_6__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_JER_EffectiveNP_6__1down
	Title: "JET JER EffectiveNP 6"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_JER_EffectiveNP_7"
	HistoFolderNameUp: JET_JER_EffectiveNP_7__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_JER_EffectiveNP_7__1down
	Title: "JET JER EffectiveNP 7"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_JER_EffectiveNP_8"
	HistoFolderNameUp: JET_JER_EffectiveNP_8__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_JER_EffectiveNP_8__1down
	Title: "JET JER EffectiveNP 8"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_JER_EffectiveNP_9"
	HistoFolderNameUp: JET_JER_EffectiveNP_9__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_JER_EffectiveNP_9__1down
	Title: "JET JER EffectiveNP 9"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_NNJvtEfficiency"
	HistoFolderNameDown: JET_NNJvtEfficiency__1down
	Samples: ttbar_FS
	HistoFolderNameUp: JET_NNJvtEfficiency__1up
	Title: "JET NNJvtEfficiency"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_Pileup_OffsetMu"
	HistoFolderNameUp: JET_Pileup_OffsetMu__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_Pileup_OffsetMu__1down
	Title: "JET Pileup OffsetMu"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_Pileup_OffsetNPV"
	HistoFolderNameUp: JET_Pileup_OffsetNPV__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_Pileup_OffsetNPV__1down
	Title: "JET Pileup OffsetNPV"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_Pileup_PtTerm"
	HistoFolderNameUp: JET_Pileup_PtTerm__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_Pileup_PtTerm__1down
	Title: "JET Pileup PtTerm"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_Pileup_RhoTopology"
	HistoFolderNameUp: JET_Pileup_RhoTopology__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_Pileup_RhoTopology__1down
	Title: "JET Pileup RhoTopology"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_PunchThrough_MC16"
	HistoFolderNameUp: JET_PunchThrough_MC16__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_PunchThrough_MC16__1down
	Title: "JET PunchThrough MC16"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "JET_SingleParticle_HighPt"
	HistoFolderNameUp: JET_SingleParticle_HighPt__1up
	Samples: ttbar_FS
	HistoFolderNameDown: JET_SingleParticle_HighPt__1down
	Title: "JET SingleParticle HighPt"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MET_SoftTrk_ResoPara"
	HistoFolderNameUp: MET_SoftTrk_ResoPara
	Samples: ttbar_FS
	Title: "MET SoftTrk ResoPara"
	Type: HISTO
	Symmetrisation: ONESIDED
	Smoothing: 40

Systematic: "MET_SoftTrk_ResoPerp"
	HistoFolderNameUp: MET_SoftTrk_ResoPerp
	Samples: ttbar_FS
	Title: "MET SoftTrk ResoPerp"
	Type: HISTO
	Symmetrisation: ONESIDED
	Smoothing: 40

Systematic: "MET_SoftTrk_Scale"
	HistoFolderNameDown: MET_SoftTrk_Scale__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MET_SoftTrk_Scale__1up
	Title: "MET SoftTrk Scale"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_ISO_BKGFRACTION"
	HistoFolderNameDown: MUON_EFF_ISO_BKGFRACTION__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_ISO_BKGFRACTION__1up
	Title: "MUON EFF ISO BKGFRACTION"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_ISO_DRMUJ"
	HistoFolderNameDown: MUON_EFF_ISO_DRMUJ__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_ISO_DRMUJ__1up
	Title: "MUON EFF ISO DRMUJ"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_ISO_LUMIUNCERT"
	HistoFolderNameDown: MUON_EFF_ISO_LUMIUNCERT__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_ISO_LUMIUNCERT__1up
	Title: "MUON EFF ISO LUMIUNCERT"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_ISO_MCXSEC"
	HistoFolderNameDown: MUON_EFF_ISO_MCXSEC__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_ISO_MCXSEC__1up
	Title: "MUON EFF ISO MCXSEC"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_ISO_MLLWINDOW"
	HistoFolderNameDown: MUON_EFF_ISO_MLLWINDOW__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_ISO_MLLWINDOW__1up
	Title: "MUON EFF ISO MLLWINDOW"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_ISO_QCDTEMPLATE"
	HistoFolderNameDown: MUON_EFF_ISO_QCDTEMPLATE__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_ISO_QCDTEMPLATE__1up
	Title: "MUON EFF ISO QCDTEMPLATE"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_ISO_SHERPA_POWHEG"
	HistoFolderNameDown: MUON_EFF_ISO_SHERPA_POWHEG__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_ISO_SHERPA_POWHEG__1up
	Title: "MUON EFF ISO SHERPA POWHEG"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_ISO_STAT"
	HistoFolderNameDown: MUON_EFF_ISO_STAT__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_ISO_STAT__1up
	Title: "MUON EFF ISO STAT"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_ISO_SUPRESSIONSCALE"
	HistoFolderNameDown: MUON_EFF_ISO_SUPRESSIONSCALE__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_ISO_SUPRESSIONSCALE__1up
	Title: "MUON EFF ISO SUPRESSIONSCALE"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_RECO_BKGFRACTION"
	HistoFolderNameDown: MUON_EFF_RECO_BKGFRACTION__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_RECO_BKGFRACTION__1up
	Title: "MUON EFF RECO BKGFRACTION"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_RECO_CR1"
	HistoFolderNameDown: MUON_EFF_RECO_CR1__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_RECO_CR1__1up
	Title: "MUON EFF RECO CR1"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_RECO_CR2"
	HistoFolderNameDown: MUON_EFF_RECO_CR2__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_RECO_CR2__1up
	Title: "MUON EFF RECO CR2"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_RECO_CR3"
	HistoFolderNameDown: MUON_EFF_RECO_CR3__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_RECO_CR3__1up
	Title: "MUON EFF RECO CR3"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_RECO_FITMODEL_LOWPT"
	HistoFolderNameDown: MUON_EFF_RECO_FITMODEL_LOWPT__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_RECO_FITMODEL_LOWPT__1up
	Title: "MUON EFF RECO FITMODEL LOWPT"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_RECO_HIGHETA_PROBEIP"
	HistoFolderNameDown: MUON_EFF_RECO_HIGHETA_PROBEIP__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_RECO_HIGHETA_PROBEIP__1up
	Title: "MUON EFF RECO HIGHETA PROBEIP"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_RECO_HIGHETA_PROBEISO"
	HistoFolderNameDown: MUON_EFF_RECO_HIGHETA_PROBEISO__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_RECO_HIGHETA_PROBEISO__1up
	Title: "MUON EFF RECO HIGHETA PROBEISO"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_RECO_LUMIUNCERT"
	HistoFolderNameDown: MUON_EFF_RECO_LUMIUNCERT__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_RECO_LUMIUNCERT__1up
	Title: "MUON EFF RECO LUMIUNCERT"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_RECO_MATCHING"
	HistoFolderNameDown: MUON_EFF_RECO_MATCHING__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_RECO_MATCHING__1up
	Title: "MUON EFF RECO MATCHING"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_RECO_MATCHING_LOWPT"
	HistoFolderNameDown: MUON_EFF_RECO_MATCHING_LOWPT__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_RECO_MATCHING_LOWPT__1up
	Title: "MUON EFF RECO MATCHING LOWPT"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_RECO_MCXSEC"
	HistoFolderNameDown: MUON_EFF_RECO_MCXSEC__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_RECO_MCXSEC__1up
	Title: "MUON EFF RECO MCXSEC"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_RECO_PTDEPENDENCY"
	HistoFolderNameDown: MUON_EFF_RECO_PTDEPENDENCY__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_RECO_PTDEPENDENCY__1up
	Title: "MUON EFF RECO PTDEPENDENCY"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_RECO_QCDTEMPLATE"
	HistoFolderNameDown: MUON_EFF_RECO_QCDTEMPLATE__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_RECO_QCDTEMPLATE__1up
	Title: "MUON EFF RECO QCDTEMPLATE"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_RECO_STAT"
	HistoFolderNameDown: MUON_EFF_RECO_STAT__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_RECO_STAT__1up
	Title: "MUON EFF RECO STAT"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_RECO_STAT_LOWPT"
	HistoFolderNameDown: MUON_EFF_RECO_STAT_LOWPT__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_RECO_STAT_LOWPT__1up
	Title: "MUON EFF RECO STAT LOWPT"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_RECO_SUPRESSIONSCALE"
	HistoFolderNameDown: MUON_EFF_RECO_SUPRESSIONSCALE__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_RECO_SUPRESSIONSCALE__1up
	Title: "MUON EFF RECO SUPRESSIONSCALE"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_RECO_SYS"
	HistoFolderNameDown: MUON_EFF_RECO_SYS__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_RECO_SYS__1up
	Title: "MUON EFF RECO SYS"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_RECO_SYS_LOWPT"
	HistoFolderNameDown: MUON_EFF_RECO_SYS_LOWPT__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_RECO_SYS_LOWPT__1up
	Title: "MUON EFF RECO SYS LOWPT"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_RECO_TAGPT"
	HistoFolderNameDown: MUON_EFF_RECO_TAGPT__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_RECO_TAGPT__1up
	Title: "MUON EFF RECO TAGPT"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_RECO_TRUTH"
	HistoFolderNameDown: MUON_EFF_RECO_TRUTH__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_RECO_TRUTH__1up
	Title: "MUON EFF RECO TRUTH"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_RECO_TRUTH_LOWPT"
	HistoFolderNameDown: MUON_EFF_RECO_TRUTH_LOWPT__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_RECO_TRUTH_LOWPT__1up
	Title: "MUON EFF RECO TRUTH LOWPT"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_TTVA_BKGFRACTION"
	HistoFolderNameDown: MUON_EFF_TTVA_BKGFRACTION__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_TTVA_BKGFRACTION__1up
	Title: "MUON EFF TTVA BKGFRACTION"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_TTVA_LUMIUNCERT"
	HistoFolderNameDown: MUON_EFF_TTVA_LUMIUNCERT__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_TTVA_LUMIUNCERT__1up
	Title: "MUON EFF TTVA LUMIUNCERT"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_TTVA_MCXSEC"
	HistoFolderNameDown: MUON_EFF_TTVA_MCXSEC__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_TTVA_MCXSEC__1up
	Title: "MUON EFF TTVA MCXSEC"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_TTVA_QCDTEMPLATE"
	HistoFolderNameDown: MUON_EFF_TTVA_QCDTEMPLATE__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_TTVA_QCDTEMPLATE__1up
	Title: "MUON EFF TTVA QCDTEMPLATE"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_TTVA_STAT"
	HistoFolderNameDown: MUON_EFF_TTVA_STAT__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_TTVA_STAT__1up
	Title: "MUON EFF TTVA STAT"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_TTVA_SUPRESSIONSCALE"
	HistoFolderNameDown: MUON_EFF_TTVA_SUPRESSIONSCALE__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_TTVA_SUPRESSIONSCALE__1up
	Title: "MUON EFF TTVA SUPRESSIONSCALE"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_TTVA_SYS"
	HistoFolderNameDown: MUON_EFF_TTVA_SYS__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_TTVA_SYS__1up
	Title: "MUON EFF TTVA SYS"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_TrigStatUncertainty"
	HistoFolderNameDown: MUON_EFF_TrigStatUncertainty__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_TrigStatUncertainty__1up
	Title: "MUON EFF TrigStatUncertainty"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_EFF_TrigSystUncertainty"
	HistoFolderNameDown: MUON_EFF_TrigSystUncertainty__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_EFF_TrigSystUncertainty__1up
	Title: "MUON EFF TrigSystUncertainty"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_ID"
	HistoFolderNameDown: MUON_ID__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_ID__1up
	Title: "MUON ID"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_MS"
	HistoFolderNameDown: MUON_MS__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_MS__1up
	Title: "MUON MS"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_SAGITTA_DATASTAT"
	HistoFolderNameDown: MUON_SAGITTA_DATASTAT__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_SAGITTA_DATASTAT__1up
	Title: "MUON SAGITTA DATASTAT"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_SAGITTA_GLOBAL"
	HistoFolderNameDown: MUON_SAGITTA_GLOBAL__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_SAGITTA_GLOBAL__1up
	Title: "MUON SAGITTA GLOBAL"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_SAGITTA_PTEXTRA"
	HistoFolderNameDown: MUON_SAGITTA_PTEXTRA__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_SAGITTA_PTEXTRA__1up
	Title: "MUON SAGITTA PTEXTRA"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_SAGITTA_RESBIAS"
	HistoFolderNameDown: MUON_SAGITTA_RESBIAS__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_SAGITTA_RESBIAS__1up
	Title: "MUON SAGITTA RESBIAS"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "MUON_SCALE"
	HistoFolderNameDown: MUON_SCALE__1down
	Samples: ttbar_FS
	HistoFolderNameUp: MUON_SCALE__1up
	Title: "MUON SCALE"
	Type: HISTO
	Symmetrisation: TWOSIDED
	Smoothing: 40

Systematic: "PRW_DATASF"
	HistoFolderNameDown: PRW_DATASF__1down
	Samples: ttbar_FS
	Title: "PRW DATASF"
	Type: HISTO
	Symmetrisation: ONESIDED
	Smoothing: 40

