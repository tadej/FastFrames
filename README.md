# FastFrames

FastFrames is a package aimed at processing ntuples produced by [TopCPToolkit](https://topcptoolkit.docs.cern.ch/) into histograms or ntuples.
FastFrames rely on ROOT's [RDataFrame](https://root.cern/doc/master/classROOT_1_1RDataFrame.html) to do the event loop processing.
The code allows users to define their own columns in a minimal way while using all the functionality of the processing.

## Documentation:

The package documentation, including detailed instruction how to compile and run the code, can be found [here](https://cern.ch/fastframes).
Doxygen documentation for the code can be found [here](https://atlas-project-topreconstruction.web.cern.ch/fastframesdoxygen/).
